import java.io.*;
import java.lang.ProcessBuilder.Redirect.Type;
import java.util.*;
import java.time.format.DateTimeFormatter;
import java.time.LocalDateTime;

public class Main {
   public static void main(String[] args) throws IOException {

      if (args.length != 3) {
         // throw an exception
         throw new IOException("You must provide 3 arguments : java Main sourceFile1 sourceFile2 targetFile");
      } else {
         try {
            ReadFile file = new ReadFile(args[0]);
            ArrayList<String[]> dataCust = file.putIntoList();

            ReadFile file2 = new ReadFile(args[1]);
            ArrayList<String[]> transaksi = file2.putIntoList();

            // Memanggil getLookup untuk menyimpan id customer yang sudah ada
            ArrayList<String> lookup = file2.getLookup();

            // Memulai proses penulisan file
            WriteFile file3;
            // Header pada file baru
            String header = "Id,Name,Date,Amount\n";
            file3 = new WriteFile(args[2], header);
            file3.combineFiles(dataCust, transaksi);

            // Program dibawah ini menghandle penambahan record ke file baru
            Scanner sc = new Scanner(System.in);
            System.out.println("Ada lagi yang ingin kamu lakukan?");
            // Tulis program untuk penambahan record
            String inputan = "halo";
            while (!inputan.equals("exit")) {
               inputan = sc.nextLine();
               if (inputan.equals("exit")) {
                  break;
               } else if (inputan.equals("tambah record")) {
                  System.out.println("Masukkan id: ");
                  int id = sc.nextInt();
                  for (int i = 0; i < lookup.size(); i++){
                     if (Integer.toString(id).equals(lookup.get(i))){
                        System.out.println("ID sudah ada. Silahkan masukkan ID lain.");
                        System.out.println("Masukkan id: ");
                        id = sc.nextInt();
                     }
                  }
                  System.out.println("Masukkan nama: ");
                  String name = sc.next();

                  System.out.println("Masukkan total harga: ");
                  int harga = sc.nextInt();

                  String tanggal = Date();
                  System.out.println("Record baru berhasil ditambahkan: " + id + "," + name + "," + tanggal + "," + harga);

                  ArrayList<String> listbaru = new ArrayList<String>();
                  listbaru.add(Integer.toString(id));
                  listbaru.add(name);
                  listbaru.add(tanggal);
                  listbaru.add(String.valueOf(harga));
                  lookup.add(Integer.toString(id));
                  file3.addRecord(listbaru);
                  System.out.println("Ada lagi yang ingin kamu lakukan?");
                  inputan = sc.nextLine();
               } else {
                  System.out.println("Perintah tidak ada. Ketik 'tambah record'");
                  continue;
               }
            }
            file3.closeFile();
         } catch (FileNotFoundException ex) {
            ex.printStackTrace();
         } catch (InputMismatchException ex) {
            System.out.println("InputMismatchException: Terdapat kesalahan format input!");
         } finally {
            System.out.println("Terima Kasih Sudah Menggunakan Program Kami!");
         }
      }

   }

   public static String Date() {
      DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd");
      LocalDateTime now = LocalDateTime.now();
      return (dtf.format(now));
   }
}
