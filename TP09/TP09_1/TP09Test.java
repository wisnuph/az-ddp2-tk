import static org.junit.Assert.*; 
import org.junit.Test;
import java.util.*;
import java.time.format.DateTimeFormatter;
import java.time.LocalDateTime;
import java.io.*;
public class TP09Test {
    
    public static String Date() {
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd");
        LocalDateTime now = LocalDateTime.now();
        return (dtf.format(now));
     }

    @Test
    public void testReadFileForFileNotFoundException(){
        assertThrows(FileNotFoundException.class, () -> {
            ReadFile nonExixtingFile = new ReadFile ("blabla.txt");
        });
        
    }

    @Test
    public void testAddRecordForInputMismatchException() throws IOException {
        String tanggal = Date();
        ArrayList<String> forTest = new ArrayList<String>();
        forTest.add("S");
        forTest.add("Ino");
        forTest.add(tanggal);
        forTest.add("25000");
        String header = "Id,Name,Date,Amount\n";
        WriteFile AddRecordTest = new WriteFile ("new.csv",header);
        assertThrows(InputMismatchException.class, () -> {
            AddRecordTest.addRecord(forTest);
        });
        
    }
}