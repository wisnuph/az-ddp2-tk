import java.util.Scanner;

public class Test{
  public static void main(String[] args){
        Scanner inp = new Scanner(System.in);
        System.out.print("Ada berapa manusia: ");
        int x = inp.nextInt();
        Manusia[] inform = new Manusia[x];
        
        for(int i=0; i<x; i++){ 
            Scanner tanya = new Scanner(System.in);
            System.out.println("Siapa Anda: ");
            String status = tanya.next().toLowerCase();
            
            System.out.println("Masukkan data-data: ");
            String nama = tanya.next();
            double uang = tanya.nextDouble();
            int umur = tanya.nextInt();
            Manusia dataManusia = new Manusia(nama, uang, umur);
            inform[i] = dataManusia;
        }

        System.out.println("-------------------");
        System.out.println("Data-data berhasil dimasukkan! Terima kasih!");
        boolean z = true;

        while(z == true){
          System.out.print("perintah: ");
          Scanner input = new Scanner(System.in);
          String perintah = input.nextLine();
          String[] q = perintah.split(" ");
          if(!perintah.equals("selesai")){
            try{
              if(q[1].toLowerCase().equals("bandingkandengan")){
                if (q[0].toLowerCase().equals(inform[0].getNama().toLowerCase()) || q[0].toLowerCase().equals(inform[1].getNama().toLowerCase())) {
                  if (q[2].toLowerCase().equals(inform[0].getNama().toLowerCase()) || q[2].toLowerCase().equals(inform[1].getNama().toLowerCase())) {          
                    if (q[0].toLowerCase().equals(q[2].toLowerCase())) {
                      if (q[0].toLowerCase().equals(inform[0].getNama().toLowerCase())) {
                        inform[0].bandingkanUang(inform[0], inform[0]);
                      }else {
                        inform[1].bandingkanUang(inform[1], inform[1]);
                      }
                    }else {
                      if (q[0].toLowerCase().equals(inform[0].getNama().toLowerCase())) {
                        inform[0].bandingkanUang(inform[0], inform[1]);
                      }else{
                        inform[0].bandingkanUang(inform[1], inform[0]);
                      }
                    }
                  } else {
                    System.out.println("Tidak ada yang bernama " + q[2] + "!");
                  }
                } else {
                  System.out.println("Tidak ada yang bernama " + q[0] + "!");
                }
              } else {
                  System.out.println("Masukkan input dengan benar!");
              }
            } catch(Exception e){
                System.out.println("Perintah tidak dapat dilakukan");
            }
          }else{
            System.out.println("Sampai jumpa!");
            z = false;
          }
        }
  }
}