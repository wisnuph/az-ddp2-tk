public class Pelanggan extends Manusia {
    private int noPelanggan;

    public Pelanggan(String nama, double uang, int umur, int noPelanggan){
        setNama(nama);
        setUang(uang);
        setUmur(umur);
        this.noPelanggan = noPelanggan;
    }
    public String beli(){
      return "Aku akan membeli minuman!";
    }

    public String toString(){
      return getNama()+", pelanggan, umur "+getUmur()+"tahun, "+"pelanggan ke-"+noPelanggan+", jumlah uang "+getUang();
    }
}
