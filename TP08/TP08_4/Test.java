import java.util.Scanner;

public class Test{
  public static void main(String[] args){
        Scanner inp = new Scanner(System.in);
        System.out.print("Ada berapa manusia: ");
        int x = inp.nextInt();
        Manusia[] inform = new Manusia[x];
        int noPelanggan=0;

        for(int i=0; i<x; i++){ 
          System.out.println("Siapa Anda: ");
          String status = inp.next().toLowerCase();
          
          System.out.println("Masukkan data-data: ");
          String nama = inp.next();
          double uang = inp.nextDouble();
          int umur = inp.nextInt();
          if (status.equals("pegawai")){
              String level = inp.next();
              Pegawai data = new Pegawai(nama, uang, umur, level);
              inform[i] = data;
          }else{
              noPelanggan++;
              Pelanggan data = new Pelanggan(nama, uang, umur,noPelanggan);
              inform[i] = data;
          }
        }

        System.out.println("-------------------");
        System.out.println("Data-data berhasil dimasukkan! Terima kasih!");
        boolean z = true;

        while(z == true){
          System.out.print("perintah: ");
          Scanner input = new Scanner(System.in);
          String perintah = input.nextLine();
          String[] q = perintah.split(" ");
          if(!perintah.equals("selesai")){
            try{
              if(q[1].toLowerCase().equals("bandingkandengan")){
                if (q[0].toLowerCase().equals(inform[0].getNama().toLowerCase()) || q[0].toLowerCase().equals(inform[1].getNama().toLowerCase())) {
                  if (q[2].toLowerCase().equals(inform[0].getNama().toLowerCase()) || q[2].toLowerCase().equals(inform[1].getNama().toLowerCase())) {          
                    if (q[0].toLowerCase().equals(q[2].toLowerCase())) {
                      if (q[0].toLowerCase().equals(inform[0].getNama().toLowerCase())) {
                        inform[0].bandingkanUang(inform[0], inform[0]);
                      }else {
                        inform[1].bandingkanUang(inform[1], inform[1]);
                      }
                    }else {
                      if (q[0].toLowerCase().equals(inform[0].getNama().toLowerCase())) {
                        inform[0].bandingkanUang(inform[0], inform[1]);
                      }else{
                        inform[0].bandingkanUang(inform[1], inform[0]);
                      }
                    }
                  } else {
                    System.out.println("Tidak ada yang bernama " + q[2] + "!");
                  }
                } else {
                  System.out.println("Tidak ada yang bernama " + q[0] + "!");
                }
              } else {
                  System.out.println("Masukkan input dengan benar!");
              }
            } catch(Exception e){
                System.out.println("Perintah tidak dapat dilakukan");
            }
          }else{
            System.out.println("Sampai jumpa!");
            System.out.println("Data-data terakhir:");
            for(int i =0; i<inform.length; i++){
              try{
                Pelanggan obj_print = (Pelanggan)inform[i];
                System.out.println((i+1)+".  "+obj_print.toString());
              }catch(Exception e){
                Pegawai obj_print = (Pegawai)inform[i];
                System.out.println((i+1)+".  "+obj_print.toString());
              }
            }
            z = false;
          }
        }
  }
}