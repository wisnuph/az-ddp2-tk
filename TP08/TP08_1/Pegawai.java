public class Pegawai extends Manusia {
    private String levelKeahlian;

    public Pegawai(String nama, double uang, int umur, String levelKeahlian){
        this.levelKeahlian = levelKeahlian;
        setNama(nama);
        setUang(uang);
        setUmur(umur);
    }
}
