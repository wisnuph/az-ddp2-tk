public class Pelanggan extends Manusia {
  private int noPelanggan;

  public Pelanggan(String nama, double uang, int umur){
      setNama(nama);
      setUang(uang);
      setUmur(umur);
  }
  public String beli(){
    return "Aku akan membeli minuman!";
  }
}
