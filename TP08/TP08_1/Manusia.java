public class Manusia
{
    private String nama;
    private double uang;
    private int umur;

    public Manusia(){
    }
    public Manusia(String nama, double uang, int umur){
        this.nama = nama;
        this.uang = uang;
        this.umur = umur;
    }
    public String getNama(){
        return nama;
    }
    public Double getUang(){
        return uang;
    }
    public int getUmur(){
        return umur;
    }
    public void setNama(String nama){
        this.nama = nama;
    }
    public void setUang(double uang){
        this.uang = uang;
    }
    public void setUmur(int umur){
        this.umur = umur;
    }
    public String bicara(){
        return "Selamat datang di kedai!";
    }
    public String berjalan(){
        return "Aku sedang berjalan!";
    }
}