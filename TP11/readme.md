# 💻 Tugas Pemrograman 11 (Kelompok)

[![License](https://img.shields.io/badge/License-BSD%203--Clause-blue.svg)](https://opensource.org/licenses/BSD-3-Clause) [![](https://img.shields.io/badge/SCeLE-Click%20Here-orange.svg)](https://scele.cs.ui.ac.id/course/view.php?id=3019) [![](https://img.shields.io/badge/LINE%20OA-Kak%20Burhan-brightgreen)](https://line.me/R/ti/p/%40157zmdwf)

**Kelas**: J | **Kode Tutor**: RF

## Topik

Wow, graphics are good !

## Dokumen Tugas

TBA

## Pembagian Tugas

| No.  | Nama | NPM  | Tugas |
| ---- | ---- | ---- | ----- |
| 1    |      |      |       |
| 2    |      |      |       |
| 3    |      |      |       |
| 4    |      |      |       |
| 5    |      |      |       |
| 6    |      |      |       |
| 7    |      |      |       |

## *Checklist*

- [ ] TBA
- [ ] TBA
- [ ] TBA
- [ ] TBA