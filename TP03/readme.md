# 💻 Tugas Pemrograman 3 (Kelompok)

[![License](https://img.shields.io/badge/License-BSD%203--Clause-blue.svg)](https://opensource.org/licenses/BSD-3-Clause) [![](https://img.shields.io/badge/SCeLE-Click%20Here-orange.svg)](https://scele.cs.ui.ac.id/course/view.php?id=3019) [![](https://img.shields.io/badge/LINE%20OA-Kak%20Burhan-brightgreen)](https://line.me/R/ti/p/%40157zmdwf)

**Kelas**: J | **Kode Tutor**: RF

## Topik

I really enjoy programming, I repeatedly do it.

## Dokumen Tugas

[https://docs.google.com/document/d/1UmB9NCDoIcMEnxhSXkDrDAFFB5tpxU9zOSecEbT_iB4/edit?usp=sharing](https://docs.google.com/document/d/1UmB9NCDoIcMEnxhSXkDrDAFFB5tpxU9zOSecEbT_iB4/edit?usp=sharing)

## Pembagian Tugas

| No.  | Nama | NPM  | Tugas |
| ---- | ---- | ---- | ----- |
| 1    |      |      |       |
| 2    |      |      |       |
| 3    |      |      |       |
| 4    |      |      |       |
| 5    |      |      |       |
| 6    |      |      |       |
| 7    |      |      |       |

## *Checklist*

- [ ] Membuat repositori tugas kelompok untuk kelompok tutor masing-masing
- [ ] Mengundang teman sekelompok dan tutor ke repositori
- [ ] Mempelajari dan memahami soal Tugas Pemrograman 3
- [ ] Mengerjakan Soal 1
- [ ] Mengerjakan Soal 2
- [ ] Mengerjakan Soal 3
- [ ] Mengumpulkan Pekerjaan pada Milestone 1
- [ ] Mengumpulkan Pekerjaan pada Milestone 2
- [ ] Mengumpulkan Pekerjaan pada Milestone 3
- [ ] Melakukan demo tugas pemrograman secara kolektif ke tutor