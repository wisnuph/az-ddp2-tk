import java.util.Random;
import java.util.Scanner;

public class TP03_3 {
    public static void main(String[] args) {
        // Konstanta
        final int STATUS_BENAR = 1;
        final int STATUS_SALAH = 0;
        final int MAX_PENGUNJUNG = 5;
        
        // Variabel Umum
        Random randomizer = new Random();
        Scanner sc = new Scanner(System.in);
        char specialChar = '1';
        String nama = "";
        String password = "";
        int jumlahPengunjung = 0;
        
        System.out.println("Selamat datang di Sistem Gerbang Kedai Kopi VoidMain!");
        while (specialChar == '1') {
            System.out.print("Apa karakter spesial untuk hari ini? (Kosongkan untuk menggunakan\nkarakter acak): ");
            // TODO: Kerjakan logika penentuan karakter spesial di sini
            String text = sc.nextLine().toUpperCase();
            if (text.equals("")) {
                int x = 65 + randomizer.nextInt(26);
                specialChar = (char) x;
            } else {
                specialChar = text.charAt(0);
                if ('A' <= specialChar && specialChar <= 'Z') {
                    break;
                } else {
                    System.out.println("Karakter harus merupakan alfabet!");
                    specialChar = '1';
                }
                // Jika dicetak masih 1, maka program pengecekan karaktermu belum tepat :)
            } 
        }
        System.out.println("Karakter spesial hari ini adalah: "+specialChar);
        System.out.println("=====PELAYANAN PENGUNJUNG======");
        System.out.println("Maksimum pengunjung saat pandemi: "+MAX_PENGUNJUNG);
        System.out.println("Untuk keluar, ketik 'exit'");
        System.out.println("===============================");

        // TODO: Kerjakan logika pengecekan nama dan kata sandi pengunjung di close
        while (nama.equals("")) {
            if (jumlahPengunjung < 5){
                System.out.println("Masukkan nama pengunjung: ");
                nama = sc.nextLine().toUpperCase();
                if (nama.equals("EXIT")) {
                    break;
                } else {
                    int maxPercobaan = 3;
                    for (int i = 1; i <= maxPercobaan; i++) {
                        System.out.println("Apa kata sandi Kedai Kopi VoidMain? ");
                        password = sc.nextLine().toUpperCase();
                        int count_specialChar = 0;
                        for (int j = 0; j < password.length(); j++) {
                            char huruf = password.charAt(j);
                            if (huruf == specialChar) {
                                count_specialChar += 1;
                            }
                        }
                        if (count_specialChar >= 3) {
                            System.out.println(nama+" berhasil masuk!");
                            nama = "";
                            jumlahPengunjung += 1;
                            break;
                        } else {
                            System.out.println("Kata sandi salah! Sisa percobaan: "+ (maxPercobaan-i));
                            if (maxPercobaan-i == 0) {
                                System.out.println(nama+" gagal masuk!");
                                nama = "";
                            }
                        }
                    }
                }
            }else{break;}
        }

        

        System.out.println("Jumlah pengunjung kedai kopi: "+jumlahPengunjung+" orang");
        System.out.println("Hari selesai!");
        sc.close(); // PENTING: Scanner perlu di-close setelah tidak digunakan.
    }
}