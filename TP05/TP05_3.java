import java.util.Scanner;

public class TP05_3 {
    public static void main (String args []) {
        Scanner sc = new Scanner(System.in);
        //soal adalah variabel untuk menampung matrix soal Dek Depe
        
        int soal[][]=new int[2][2]; 
        int det = 0;
        for(int row = 0;row < soal.length;row++){
            for(int column = 0;column < soal[row].length; column++){
                soal[row][column] = sc.nextInt();
            }
        }
        det = determinant(soal);
        System.out.println(invertAble(det));
    }
    public static int determinant(int[][] matrix) {
        int digLeft = 1; int digRight = 1;
        for(int i=0;i<matrix.length;i++)
            for(int j=0;j<matrix.length;j++)
                if(i==j)
                    digLeft *= matrix[i][j];
                else
                    digRight *= matrix[i][j];
        return (digLeft-digRight);
    } 
    public static boolean invertAble(int determinan) {
        if (determinan == 0)
            return false;
        else
            return true;
    }
}
