import java.util.Scanner;
import java.util.Arrays; 
public class TP05_2 {
    public static void main(String args[]) {
        System.out.println("Masukkan 9 nomor antrean!");
        Scanner sc = new Scanner(System.in);
        int antrianAll[][]=new int[3][3];
        //antrian1, antrian2, antrian3 digunakan untuk
        int antrian1[] = new int[3];
        int antrian2[] = new int[3];
        int antrian3[] = new int[3];
        for (int i = 0; i<9; i++){
            int barisan = sc.nextInt();
            if (i>=0 && i<3){
                antrian1[i] = barisan;
                antrianAll[0] = antrian1;
            }else if(i>=3 && i<6){
                antrian2[i-3] = barisan;
                antrianAll[1] = antrian2;
            }else{
                antrian3[i-6] = barisan;
                antrianAll[2] = antrian3;
                
            }
        }
        print(antrianAll);
    }

    /**
   * Metode ini untuk mencetak jawaban hasil antrean
   * @param antrian adalah array dua dimensi 3x3 yang 
   * @return none Metode ini tidak mengembalikan apa pun
   */
    public static void print(int[][] antrian) {
        System.out.println("Antreannya adalah: ");
        //TODO : lengkapi method ini
        for (int baris = 0; baris<antrian.length; baris++){
            Arrays.sort(antrian[baris]);
            for (int kolom = 0; kolom<antrian[baris].length; kolom++){
                if(antrian[baris][kolom]>9){
                    System.out.print(antrian[baris][kolom] + "     ");
                }
                else{
                    System.out.print(antrian[baris][kolom] + "      ");
                }
                // System.out.print(antrian[baris][kolom] + "      ");
            }
            System.out.println();
        }
    }
}

