# 💻 Tugas Pemrograman 5 (Kelompok)

[![License](https://img.shields.io/badge/License-BSD%203--Clause-blue.svg)](https://opensource.org/licenses/BSD-3-Clause) [![](https://img.shields.io/badge/SCeLE-Click%20Here-orange.svg)](https://scele.cs.ui.ac.id/course/view.php?id=3019) [![](https://img.shields.io/badge/LINE%20OA-Kak%20Burhan-brightgreen)](https://line.me/R/ti/p/%40157zmdwf)

**Kelas**:  | **Kode Tutor**: 

## Topik

We are stronger as one. 

## Dokumen Tugas

https://docs.google.com/document/d/1CkOlTeZ0ecC-WZssUjP6LC9IxV3_znkQLu7-sijtOCs/edit#

## Pembagian Tugas

| No.  | Nama                | NPM         | Tugas  |
| ---- | ------------------- | ------------| ------ |
| 1    | Ahmad Dzikrul Fikri | 1806196806  | TP05_2 |
| 2    | Devia Febyanti      | 1806136706  | TP05_3 |
| 3    | Wisnu PH            | 1806197342  | TP05_3 |
| 4    |                     |             |        |
| 5    |                     |             |        |
| 6    |                     |             |        |
| 7    |                     |             |        |

## *Checklist*

- [ ] Membaca Soal 1
- [ ] Mengerjakan Soal 2
- [ ] Mengerjakan Soal 3
- [ ] Melaporkan kepada asdos terkait progress pada Milestone 1
- [ ] Melaporkan kepada asdos terkait progress pada Milestone 2
- [ ] Melaporkan kepada asdos terkait progress pada Milestone 3
- [ ] Melakukan demo secara kolektif