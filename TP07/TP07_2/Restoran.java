import java.util.Scanner;
import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;
import java.util.Queue;


public class Restoran{  // TODO: Modifikasi kelas Restoran sehingga Simulator.java bisa berjalan
    private Burhan burhan;
    private double kas = 0.0;
    private double idxK;
    private Queue<Pelanggan> antrian = new LinkedList<>();
    private HashMap<String, Integer> menu;
    private boolean buka;

    public Restoran(){   
        this.burhan = new Burhan(10000.0);
        this.kas = 0.0;
        this.idxK = 100.0;
        System.out.println("\n=====Kedai VoidMain telah dibuka=====");
        System.out.println("Silakan masukkan perintah:");
    }
    public Restoran(double kas){   
        this.burhan = new Burhan(10000.0);
        this.kas = kas;
        this.idxK = 100.0;
        System.out.println("\n=====Kedai VoidMain telah dibuka=====");
        System.out.println("Silakan masukkan perintah:");
    }
    public Restoran(double kas, double uangBurhan){   
        this.burhan = new Burhan(uangBurhan);
        this.kas = kas;
        this.idxK = 100.0;
        System.out.println("\n=====Kedai VoidMain telah dibuka=====");
        System.out.println("Silakan masukkan perintah:");
    }
    public void parseMenu(String menu){  // TODO: Implementasikan method untuk memasukkan menu dari file txt menjadi object!
        // Hint: cek Seeder.java :D
        try{
            File myFile = new File(menu);
            Scanner baca = new Scanner(myFile);

            HashMap<String, Integer> isiMenu = new HashMap<String, Integer>();
            
            while(baca.hasNextLine()){
                String data = baca.nextLine().toLowerCase();
                isiMenu.put(data.substring(0,data.indexOf(" :")),Integer.parseInt(data.substring(data.indexOf(": ")+2)));
            }
            this.menu = isiMenu;
        }catch(Exception e){
            System.out.println("File not found");
        }
    }
    public void isiMenu(){
        for(String t : this.menu.keySet()){
            System.out.printf("%-29s%10s\n", t, this.menu.get(t)+"DDD");
        }
    }
    public void layaniPelanggan(Bon o){  // TODO: Implementasikan method untuk melayani semua pelanggan di antrian!
        System.out.println("Restoran sedang melayani pelanggan.");
        parseMenu("menu.txt");
        
        Scanner inp = new Scanner(System.in);
        
        ArrayList<String[]> isiPesanan = new ArrayList<>();
        ArrayList<String[]> rekapPesanan = new ArrayList<>();
        int idx = 0;
        
        for(Pelanggan obj : this.antrian){
            System.out.println("\n==============Daftar Menu==============\n");
            isiMenu();
            System.out.println("\n==============Daftar Menu==============\n");
            System.out.println("Pelanggan " + obj.getNama() + " memesan:");
            
            boolean layani = true;
            while (layani == true){  
                String isi = inp.nextLine().toLowerCase();

                if (!isi.equals("")){
                    if (this.menu.get(isi.substring(0, isi.lastIndexOf(" "))) == null){
                        System.out.println("Pesanan tidak ada di daftar Menu, mohon coba lagi.");
                    }else{
                        String[] arr = new String[3];
                        arr[0] = isi.substring(0, isi.lastIndexOf(" "));
                        arr[1] = isi.substring(isi.lastIndexOf(" ")+1);
                        arr[2] = Double.toString((Integer.parseInt(arr[1]))*menu.get(arr[0]));
                        this.idxK -= Integer.parseInt(arr[1]);
                        isiPesanan.add(arr); rekapPesanan.add(arr);
                        System.out.print("Ingin pesan lagi? (ya/tidak): ");
                        String jwb = inp.next();

                        if (jwb.equalsIgnoreCase("ya")){
                            layani = true;
                        }else{
                            o.setHargaTotal(isiPesanan, idxK);
                            o.setHargaTotalAll();
                            o.parseDaftarPesanan(isiPesanan);
                            System.out.println("Pesanan selesai");
                            System.out.println("Total harga: " + o.hargaTotal + "DDD");
                            this.kas += o.hargaTotal;
                            layani = false; isiPesanan.clear();
                        }
                    }
                }
            }
            if(obj.getUang() >= o.hargaTotal){ 
                obj.bayar(burhan, o);
                System.out.println("Uang pelanggan " + obj.nama + " cukup, bon lunas.");
            }else{ 
                System.out.print("Uang pelanggan " + obj.nama + " tidak cukup! Apakah Kak Burhan mau membantu? (ya/tidak) ");
                String jwb = inp.next();
                if (jwb.equalsIgnoreCase("ya")){
                    System.out.print("Kak Burhan mau membantu sebanyak berapa? ");
                    double n = inp.nextDouble();
                    if(n >= (o.hargaTotal - obj.getUang())){
                        obj.mintaBantuan(burhan, obj, n);
                        System.out.println("Uang yang terkumpul sudah cukup, bon lunas.");
                    }else{
                        System.out.println("Uang tidak cukup, pesanan dibatalkan");
                    }
                }else{
                    System.out.println("Uang tidak cukup, pesanan dibatalkan");
                }
            }
            if (idx < this.antrian.size()-1){
                System.out.println("Pesanan telah diberikan, pelanggan selanjutnya silakan memesan.");
            }else{
                System.out.println("Pesanan telah diberikan, antrian kosong.");
            }
            idx += 1;
        }
        o.setRekapTotalMenu(rekapPesanan);
        o.setRekapAkhir();
        System.out.println("\n=====Rekap Pendapatan=====");
        System.out.println("Total pendapatan: " + o.getHargaTotalAll() + "DDD");
        System.out.println("Menu yang terjual pada sesi ini:");
        System.out.println(o.toString());
        System.out.println("=====Rekap Pendapatan=====\n");
    }    

    public void reset() { // TODO: Implementasikan method untuk mereset kedai sesuai permintaan soal!
        this.buka = true;
        this.burhan = new Burhan(10000.0);
        this.kas = 0.0;
        this.idxK = 100.0;
    }
    public void tutupRestoran(Bon o) { // TODO: Implementasikan method untuk rekap total pendapatan secara keseluruhan!
        this.buka = false;
        System.out.println("=======Rekap Pendapatan Akhir=======");
        System.out.println("Total pendapatan akhir: " + this.kas + "DDD");
        System.out.println("Menu yang terjual secara keseluruhan:");
        System.out.println(o.toString(o.getRekapAkhir()));
        System.out.println("=======Rekap Pendapatan Akhir=======");
        System.out.println("Kedai sudah ditutup. Sampai jumpa besok!");
    }
    public Burhan getBurhan() {
        return this.burhan;
    }
    public Queue<Pelanggan> getAntrian() {
        return this.antrian;
    }
    public String getDaftarAntrian() {
        return Arrays.toString(this.antrian.toArray());
    }
    public Pelanggan getPelangganTerdepan() {
        return this.antrian.poll();
    }
    public void tambahAntrianPelanggan(Pelanggan p){
        this.antrian.add(p);
    }
    public void hapusAntrianPelanggan(){
        this.antrian.clear();
    }
    public double getKas() {
        return this.kas;
    }
    public void setKas(double kas) {
        this.kas = kas;
    }
    public double getIdxK(){
        return this.idxK;
    }
    public void setIdxK(double idxK) {
        this.idxK = idxK;
    }
    @Override
    public String toString() {
        return "Kedai voidMain, dijaga solo oleh Kak Burhan. Total uang kas sekarang ="+Double.toString(kas);
    }
    public static void print(String s) {
        System.out.println(s);
    }
    public static void print(int i) {
        System.out.println(i);
    }
    public static void print(double i) {
        System.out.println(i);
    }
}