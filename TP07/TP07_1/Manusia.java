

import java.util.ArrayList;

public class Manusia {
    private String nama;
    ArrayList<String> listPesan = new ArrayList<>();
    public Manusia(String nama){
        this.nama = nama;
    }
    public void kirimPesan(Manusia m, String pesan){
        m.listPesan.add("Saya berkata: "+ pesan + " ke " + m.getNama());
        System.out.println(this.nama + " berkata: "+ pesan + " ke " + m.nama);
    }
    public void cetakSemuaPesan(){
        for (String i : listPesan) {
            System.out.println(i);
        }
    }
    public String getNama(){
        return this.nama;
    }
}
